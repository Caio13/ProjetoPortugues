package Control;

import Model.FlashCard;
import Model.Materia;
import Model.Usuario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ControleMateria {
    
    private final ArrayList<Materia> listaMaterias;

    public ControleMateria() {
        this.listaMaterias = new ArrayList<>();
    }
    
    public ControleMateria(ArrayList listaMaterias){
        this.listaMaterias = listaMaterias;
    }
        
    /*
     * @param codigo o código do materia a ser recuperado
     * @return o materia encontrado, ou null caso o materia não exista na lista
     */
    public Materia pesquisa(int codigo){
        for(Materia materia : listaMaterias){
            if(materia.getCodigo() == codigo){
                return materia;
            }
        }
        return null;
    }    
    
     /*
     * Insere um novo materia na lista
     * @param codigo o código do materia a ser inserido
     * @param descricao a descricao do materia
     * @param preco o preco do materia
     * @return o novo materia inserido, ou null caso o materia já exista na lista
     */
    public Materia insere(int codigo, String nome){
        //verifica se o materia já existe na lista
        if(pesquisa(codigo) == null){
            Materia materia = new Materia(nome, codigo);
            listaMaterias.add(materia);
            //gravaBancoMateria(materia);
            return materia;
        }else{
            return null;                     
        }        
    }
    
    public FlashCard insereFlashCard(int codigo, String pergunta, String resposta){
        Materia materia;
        materia = pesquisa(codigo);
        //verifica se o materia já existe na lista
        if(materia != null){
            FlashCard flashCard = new FlashCard(pergunta, resposta);
            return materia.addListaFlashCards(flashCard);
        }else{
            return null;                     
        }        
    }
    
    public Materia remove(int codigo){
        Materia materia;
        for(int i=0; i<listaMaterias.size(); i++){
            materia = listaMaterias.get(i);
            if(materia.getCodigo() == codigo){
                return listaMaterias.remove(i);
            }
        }
        return null;
    }
    
    //altera o materia cadastrado na lista com o código do parâmetro
    public Materia alteraNome(int codigo, String nome){
        Materia materia = pesquisa(codigo);
        if(materia != null){
            materia.setNome(nome);
            return materia;
        }
        return null;        
    }

    public ArrayList<Materia> listaTodos(){
        return listaMaterias;
    }
    
    /*public void gravaBancoMateria(Materia materia){
        File file = new File("usuarios.txt");
        int codMateria;
        try {
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            
            bw.write(materia.getNome()+"\n"+materia.getCodigo()+"\n\n");
            bw.close();
            fw.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar!");
        }
    }*/
}