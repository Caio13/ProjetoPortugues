package Control;

import Model.Materia;
import Model.TipoUsuario;
import Model.Usuario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ControleUsuario {
    
    private final ArrayList<Usuario> listaUsuarios;
    
    public ControleUsuario(ArrayList listaUsuarios){
        this.listaUsuarios = listaUsuarios;
    }

    public ControleUsuario() {
        this.listaUsuarios = new ArrayList<>();
    }
        
    /*
     * @param codigo o código do usuario a ser recuperado
     * @return o usuario encontrado, ou null caso o usuario não exista na lista
     */
    public Usuario pesquisa(String login){
        for(Usuario usuario : listaUsuarios){
            if(usuario.getLogin().equals(login)){
                return usuario;
            }
        }
        return null;
    }    
    
     /*
     * Insere um novo usuario na lista
     * @param codigo o código do usuario a ser inserido
     * @param descricao a descricao do usuario
     * @param preco o preco do usuario
     * @return o novo usuario inserido, ou null caso o usuario já exista na lista
     */
    public Usuario insere(String nome, String login, String senha, TipoUsuario tipoUsuario){
        //verifica se o usuario já existe na lista
        if(pesquisa(login) == null){
            Usuario usuario = new Usuario(nome, login, senha, tipoUsuario);
            listaUsuarios.add(usuario);
            //gravaBancoUsuario(usuario);
            return usuario;
        }else{
            return null;                     
        }        
    }
    
    
    public Usuario remove(String login){
        Usuario usuario;
        for(int i=0; i<listaUsuarios.size(); i++){
            usuario = listaUsuarios.get(i);
            if(usuario.getLogin().equals(login)){
                return listaUsuarios.remove(i);
            }
        }
        return null;
    }
    
   //altera o usuario cadastrado na lista com o código do parâmetro
    public Usuario alteraSenha(String login, String senhaAtual, String senhaNova){
        Usuario usuario = pesquisa(login);
        if(usuario != null){
            if(usuario.getSenha().equals(senhaAtual)){
                usuario.setSenha(senhaNova);
                return usuario;
            } 
        }
        return null;        
    }

    public Usuario autenticaUsuario(String login, String senha){
        Usuario usuario = pesquisa(login);
        if(usuario != null){
            if(!senha.equals(usuario.getSenha())){
                usuario = null;
            }
        }
        return usuario;
    }
    
    /* public void gravaBancoUsuario(Usuario usuario){
        File file = new File("usuarios.txt");
        int codMateria;
        try {
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            
            bw.write(usuario.getNome()+"\n"+usuario.getTipoUsuario().getTipoUsuario()+"\n"+usuario.getLogin()+"\n"+usuario.getSenha()+"\n");
            for(Materia materia : usuario.listaMaterias()){
                codMateria = materia.getCodigo();
                bw.write(codMateria+" ");
            }
            bw.write("\n\n");
            bw.close();
            fw.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar!");
        }
    }*/
    
    public ArrayList<Usuario> listaTodos(){
        return listaUsuarios;
    }
}
