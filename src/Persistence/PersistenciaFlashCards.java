/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Control.ControleMateria;
import Model.FlashCard;
import Model.Materia;
import Model.NivelDificuldade;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author caiov
 */
public class PersistenciaFlashCards {
    
    ControleMateria controleMateria;
    
    public PersistenciaFlashCards(ControleMateria controleMateria) {
        this.controleMateria = controleMateria;
    }
    
    public void lerBanco(){
        File file = new File("flashcards.txt");
        FileReader Arq = null;
        int i;
        String pergunta, resposta, codmateria, dificuldade, dados;
        FlashCard flashCard = null;
        Materia materia;
        try {
            Arq = new FileReader(file);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
            return;
        }
        BufferedReader inputArq = new BufferedReader(Arq);
        try {
            while((dados = inputArq.readLine()) != null){
                pergunta = dados;
                dados = inputArq.readLine();
                resposta = dados;
                dados = inputArq.readLine();
                dificuldade  = dados;
                dados = inputArq.readLine();
                codmateria  = dados;
                dados = inputArq.readLine();
                if(dificuldade.equalsIgnoreCase("fácil")) flashCard = new FlashCard(pergunta, resposta, NivelDificuldade.FACIL);
                else if(dificuldade.equalsIgnoreCase("difícil")) flashCard = new FlashCard(pergunta, resposta, NivelDificuldade.DIFICIL);
                else if(dificuldade.equalsIgnoreCase("mediano")) flashCard = new FlashCard(pergunta, resposta, NivelDificuldade.MEDIANO);
                materia = controleMateria.pesquisa(Integer.parseInt(codmateria));
                materia.addListaFlashCards(flashCard);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
        }
    }
    
    public void gravaBancoFlashCards(){
        File file = new File("usuarios.txt");
        ArrayList<Materia> listaMateria = controleMateria.listaTodos();
        try {
            FileWriter fw = new FileWriter(file,false);
            BufferedWriter bw = new BufferedWriter(fw);
            for(Materia materia : listaMateria){
                for(FlashCard flashCard : materia.listaFlashCards()){
                    bw.write(flashCard.getPergunta()+"\n"+flashCard.getResposta()+"\n"+flashCard.getNivelDificuldade().getDificuldade()+"\n"+materia.getCodigo()+"\n\n");
                }
            }
            bw.close();
            fw.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar!");
        }
    }
}
