/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.Materia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author caiov
 */
public class PersistenciaMaterias {

    public PersistenciaMaterias() {
    }
    
    public ArrayList<Materia> lerBanco(){
        File file = new File("materias.txt");
        FileReader Arq = null;
        int i;
        String nome, codigo, dados;
        Materia materia;
        ArrayList<Materia> listaMateria = new ArrayList();
        try {
            Arq = new FileReader(file);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
            return null;
        }
        BufferedReader inputArq = new BufferedReader(Arq);
        try {
            while((dados = inputArq.readLine()) != null){
                nome = dados;
                dados = inputArq.readLine();
                codigo = dados;
                dados = inputArq.readLine();
                materia = new Materia(nome, Integer.parseInt(codigo));
                listaMateria.add(materia);
            }
            return listaMateria;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
        }
        return null;
    }
}
