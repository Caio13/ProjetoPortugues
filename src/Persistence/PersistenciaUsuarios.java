/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Control.ControleMateria;
import Model.Materia;
import Model.TipoUsuario;
import Model.Usuario;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author caiov
 */
public class PersistenciaUsuarios {
    ControleMateria controleMateria;

    public PersistenciaUsuarios(ControleMateria controleMateria) {
        this.controleMateria = controleMateria;
    }
    
    public ArrayList<Usuario> lerBanco(){
        File file = new File("usuarios.txt");
        FileReader Arq = null;
        int i;
        String dados, nome, tipoUsuario, login, senha;
        Materia materia;
        ArrayList<Materia> listaMateria = new ArrayList();
        Usuario usuario = new Usuario();
        ArrayList<Usuario> listaUsuario = new ArrayList();
        try {
            Arq = new FileReader(file);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
            return null;
        }
        BufferedReader inputArq = new BufferedReader(Arq);
        try {
            while((dados = inputArq.readLine()) != null){
                nome = dados;
                dados = inputArq.readLine();
                tipoUsuario = dados;
                dados = inputArq.readLine();
                login = dados;
                dados = inputArq.readLine();
                senha = dados;
                dados = inputArq.readLine();
                String[] split = dados.split(" ");
                i = 0;
                while(split[i] != null){
                    materia = controleMateria.pesquisa(Integer.parseInt(split[i]));
                    listaMateria.add(materia);
                }
                dados = inputArq.readLine();
                if(tipoUsuario.equalsIgnoreCase("professor")) usuario = new Usuario(nome, TipoUsuario.PROFESSOR, login, senha, listaMateria);
                else if(tipoUsuario.equalsIgnoreCase("aluno")) usuario = new Usuario(nome, TipoUsuario.ALUNO, login, senha, listaMateria);
                else if(tipoUsuario.equalsIgnoreCase("administrador")) usuario = new Usuario(nome, TipoUsuario.ADMINISTRADOR, login, senha, listaMateria);
                listaUsuario.add(usuario);
            }
            return listaUsuario;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar o programa, verifique o banco de dados!");
        }
        return null;
    }
}
