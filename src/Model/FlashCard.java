package Model;

public class FlashCard {
    
    private String pergunta;
    private String resposta;
    private NivelDificuldade nivelDificuldade;

    public FlashCard(String pergunta, String resposta, NivelDificuldade nivelDificuldade) {
        this.pergunta = pergunta;
        this.resposta = resposta;
        this.nivelDificuldade = nivelDificuldade;
    }
    
    public FlashCard(String pergunta, String resposta) {
        this.pergunta = pergunta;
        this.resposta = resposta;
        nivelDificuldade = NivelDificuldade.DIFICIL;
    }

    public String getPergunta() {
            return pergunta;
    }

    public String getResposta() {
            return resposta;
    }

    public NivelDificuldade getNivelDificuldade() {
            return nivelDificuldade;
    }

    private void setPergunta(String pergunta) {
            this.pergunta = pergunta;
    }

    private void setResposta(String resposta) {
            this.resposta = resposta;
    }

    private void setNivelDificuldade(NivelDificuldade nivelDificuldade) {
            this.nivelDificuldade = nivelDificuldade;
    }
    
    public void alteraPergunta(String pergunta){
        setPergunta(pergunta);      
    }
    
    public void alteraResposta(String resposta){
        setResposta(resposta);      
    }
    
    public void alteraDificuldade(NivelDificuldade nivelDificuldade){
        setNivelDificuldade(nivelDificuldade); 
    }
}
