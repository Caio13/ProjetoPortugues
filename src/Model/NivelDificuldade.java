package Model;

public enum NivelDificuldade {
    DIFICIL("Difícil"),
    MEDIANO("Mediano"),
    FACIL("Fácil");
    
    private String dificuldade;

    private NivelDificuldade(String dificuldade) {
        this.dificuldade = dificuldade;
    }

    public String getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(String dificuldade) {
        this.dificuldade = dificuldade;
    }
    
    
}
