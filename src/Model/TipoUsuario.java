package Model;

public enum TipoUsuario {
    ADMINISTRADOR("Administrador"), PROFESSOR("Professor"), ALUNO("Aluno");
    
    private String tipoUsuario;

    private TipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

}
