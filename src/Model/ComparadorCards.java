package Model;

import java.util.Comparator;

public class ComparadorCards implements Comparator<FlashCard> {

    @Override
    public int compare(FlashCard o1, FlashCard o2) {
        if(o1.getNivelDificuldade().ordinal()< o2.getNivelDificuldade().ordinal()) return 1;
        else if (o1.getNivelDificuldade().ordinal() > o2.getNivelDificuldade().ordinal()) return -1;
        else return 0;
    }
    
}
