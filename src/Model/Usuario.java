package Model;

import java.util.ArrayList;

public class Usuario {

    private String nome;
    private TipoUsuario tipoUsuario;
    private String login;
    private String senha;
    private ArrayList<Materia> listaMaterias;

    public Usuario() {
    }
    
    public Usuario(String nome, TipoUsuario tipoUsuario, String login, String senha, ArrayList<Materia> listaMaterias) {
        this.nome = nome;
        this.tipoUsuario = tipoUsuario;
        this.login = login;
        this.senha = senha;
        this.listaMaterias = listaMaterias;
    }
    
    public Usuario(String nome, String login, String senha, TipoUsuario tipoUsuario) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.tipoUsuario = tipoUsuario;
        this.listaMaterias = new ArrayList();
    }

    public String getNome() {
        return nome;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public String getLogin() {
        return login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public Materia adicionaMateriaGrade(Materia materia){
        for(Materia m : listaMaterias){
            if(m.getCodigo() == materia.getCodigo()){
                return null;
            }
        }
        //Materia materia = new Materia(nome, codigo);
        listaMaterias.add(materia);
        return materia;                     
    }
    
    public Materia removeMateriaGrade(int codigo){
        Materia materia;
        for(int i=0; i<listaMaterias.size(); i++){
            materia = listaMaterias.get(i);
            if(materia.getCodigo() == codigo){
                return listaMaterias.remove(i);
            }
        }
        return null;
    }
    
    public ArrayList<Materia> listaMaterias(){
        return listaMaterias;
    }
}
