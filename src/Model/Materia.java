package Model;

import java.util.ArrayList;
import java.util.Collections;

public class Materia {

    private String nome;
    private final int codigo;
    private ArrayList<FlashCard> listaFlashCards;

    public Materia(String nome, int codigo) {
        this.nome = nome;
        this.codigo = codigo;
        this.listaFlashCards = new ArrayList();
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return nome;
    }

    public int getCodigo() {
        return codigo;
    }
    public FlashCard addListaFlashCards(FlashCard flashCard){
        listaFlashCards.add(flashCard);
        ordenaFlashCards();
        return flashCard;
    }
    public ArrayList<FlashCard> listaFlashCards(){
        return listaFlashCards;
    }
    
    public void ordenaFlashCards(){
        Collections.sort(listaFlashCards, new ComparadorCards());
    }
    
    public FlashCard remove(int indice){
        return listaFlashCards.remove(indice);
    }
}
